
public class Book {

private final String title;
	
	/**
	 * Create a new Book object with the given information.
	 * 
	 * @param title
	 *            the title of the character
	 */
	public Book(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}

	@Override
	public boolean equals(Object obj) {
		// checks if the object is an instance of book
		if (obj instanceof Book) {
			// now checks and returns whether the title of the object is the same as this
			return ((Book) obj).getTitle() == this.getTitle();
		}
		// if object is not an instance of book returns false
		return false;
	}
	
	@Override
	public int hashCode() {
		final int prime = 37;
		// takes the length of the character name string and multiply by prime
		return prime * this.getTitle().toCharArray().length;
	}
}
