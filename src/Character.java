
public class Character {
	private final String name;
	public boolean visited;
	
	/**
	 * Create a new Character object with the given information.
	 * 
	 * @param name
	 *            the name of the character
	 */
	public Character(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public Boolean isVisited(){
		return visited;
	}

	@Override
	public boolean equals(Object obj) {
		// checks if the object is an instance of character
		if (obj instanceof Character) {
			// now checks and returns whether the name of the object is the same as this
			return ((Character) obj).getName() == this.getName();
		}
		// if object is not an instance of character returns false
		return false;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		// takes the length of the character name string and multiply by prime
		return prime * this.getName().toCharArray().length;
	}
}
