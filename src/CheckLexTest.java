import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


public class CheckLexTest {

	ArrayList<String> realBetterList = new ArrayList<String>();
	ArrayList<String> fakeBetterList = new ArrayList<String>();
	ArrayList<String> badSizeList = new ArrayList<String>();
	ArrayList<String> compareList = new ArrayList<String>();
	
	@Before
	public void setup() {
		
		// declare some strings alphabetically
		String a = "avocado";
		String b = "banana";
		String c = "cherry";
		String d = "durian";
		String e = "eggfruit";
		String f = "fig";
		String g = "grape";
		String h = "honeydew";
		String i = "iceberg lettuce";
		String j = "jujube";
		String k = "kiwi";
		String l = "lime";
		String m = "mango";
		String n = "nectarine";
		String o = "olive";
		String p = "pomegranate";
		String q = "quince";
		String r = "rambutan";
		String s = "starfruit";
		String t = "tangarine";
		String u = "ugli";
		String v = "voavanga";
		String w = "watermelon";
		String x = "ximenia";
		String y = "yam";
		String z = "zucchini";
		
		// add 13 strings each to the lists except for badList (10 lexicographically better strings)
		
		realBetterList.add(a);
		realBetterList.add(b);
		realBetterList.add(c);
		realBetterList.add(d);
		realBetterList.add(e);
		realBetterList.add(f);
		realBetterList.add(g);
		realBetterList.add(h);
		realBetterList.add(i);
		realBetterList.add(j);
		realBetterList.add(k);
		realBetterList.add(l);
		realBetterList.add(m);
		
		fakeBetterList.add(a);
		fakeBetterList.add(b);
		fakeBetterList.add(c);
		fakeBetterList.add(d);
		fakeBetterList.add(e);
		fakeBetterList.add(f);
		fakeBetterList.add(g);
		fakeBetterList.add(h);
		fakeBetterList.add(i);
		fakeBetterList.add(z); // fail here
		fakeBetterList.add(k);
		fakeBetterList.add(l);
		fakeBetterList.add(m);
		
		badSizeList.add(a);
		badSizeList.add(b);
		badSizeList.add(c);
		badSizeList.add(d);
		badSizeList.add(e);
		badSizeList.add(f);
		badSizeList.add(g);
		badSizeList.add(h);
		badSizeList.add(i);
		badSizeList.add(j);
		
		compareList.add(n);
		compareList.add(o);
		compareList.add(p);
		compareList.add(q);
		compareList.add(r);
		compareList.add(s);
		compareList.add(t);
		compareList.add(u);
		compareList.add(v);
		compareList.add(w);
		compareList.add(x);
		compareList.add(y);
		compareList.add(z);
	}
	
	@Test
	public void trueCheckLex() {
		assertTrue(checkLex(realBetterList, compareList));
	}
	
	@Test
	public void falseCheckLex() {
		assertFalse(checkLex(fakeBetterList, compareList));
	}
	
	@Test
	public void badSizeCheckLex() {
		assertFalse(checkLex(badSizeList, compareList));
	}
	
	/**
	 * Takes a pair of equally sized lists, the first assuming to be the
	 * lexicographically better (appears first in a dictionary) than the second
	 * list with each respective String. Returns true/false based on this
	 * assumption.
	 * 
	 * @param bestList: the assumed lexicographically better list
	 * @param list: the list used to compare bestList
	 * @return: true if the bestList is lexicographically better in respective
	 *          String elements and false otherwise
	 */
	private static boolean checkLex(List<String> bestList,
			List<String> list) {
		// assumptiong starts off true
		boolean isBetter = true;
		// if the two lists are not of equal size then assumption fails due to RI failure
		if(bestList.size() != list.size()) {
			isBetter = false;
		}
		// once size equality passes, checks that each string of bestList is lexicographically better than list
		else for(int index = 0; index < list.size(); index++) {
			// if the current element of list is better than bestList, assumption fails
			if(list.get(index).compareTo(bestList.get(index)) < 0) {
				isBetter = false;
			}
		}
		return isBetter;
	}
}
