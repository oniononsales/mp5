import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Iterator {

	private boolean next;
	private BufferedReader inputReader;
	private String nextRow;
	private String name;
	private String title;

	/**
	 * Create an Iterator given a filename.
	 * 
	 * @param fileName
	 *            the name of the file to open.
	 * @throws IOException
	 *             if there was a problem reading the file.
	 */
	public Iterator(String fileName) throws IOException {

		// open the file
		inputReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));

		// read the first line and set the state of the iterator
		if ((nextRow = inputReader.readLine()) != null) {
			next = true;
		} else {
			next = false;
		}
	}

	/**
	 * Check if there is more data that the iterator can process.
	 * 
	 * @return true if there is more data otherwise return false.
	 */
	public boolean hasNext() {
		return next;
	}

	/**
	 * Return the next character/book from the iterator.
	 * 
	 * @return the next character in the file. Requires that hasNext() is
	 *         true otherwise null is returned.
	 */
	public ArrayList<Object> getNextObjs() throws IOException {

		if (!next) {
			// we are the end of the stream so return null.
			// ideally the client should have checked hasNext() and this should
			// not be necessary
			return null;
		} else {

			// split the nextRow string at the tabs to create columns
			String[] columns = nextRow.split("\t");

			// the zeroth column represents the character name
			name = columns[0];
			// the first column represents the book title
			title = columns[1];

			// advance the iterator state with respect to the input stream
			if ((nextRow = inputReader.readLine()) != null) {
				next = true;
			} else {
				next = false;
			}
			// index 0 contains the character object and index 1 contains the book object
			ArrayList<Object> pair = new ArrayList<>();
			pair.add(new Character(name));
			pair.add(new Book(title));
			
			return pair;
		}
	}
	
	/**
	 * Return the next name/title from the iterator.
	 * 
	 * @return the next name in the file. Requires that hasNext() is
	 *         true otherwise null is returned.
	 */
	public String[] getNextStrings() throws IOException {

		if (!next) {
			// we are the end of the stream so return null.
			// ideally the client should have checked hasNext() and this should
			// not be necessary
			return null;
		} else {

			// split the nextRow string at the tabs to create columns
			String[] columns = nextRow.split("\t");
			// the zeroth column represents the character name
			// the first column represents the book title

			// advance the iterator state with respect to the input stream
			if ((nextRow = inputReader.readLine()) != null) {
				next = true;
			} else {
				next = false;
			}

			return columns;
		}
	}
}