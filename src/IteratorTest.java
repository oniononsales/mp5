import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


public class IteratorTest {

	Iterator objectIter;
	Iterator stringIter;
	
	@Before
	public void setup() throws IOException {
		objectIter = new Iterator("labeled_edges.tsv");
		stringIter = new Iterator("labeled_edges.tsv");
	}
	
	@Test
	public void getNextObjsTest() throws IOException {
		int timer = 0;
		// while iterating allow the iterator to run for only 10 cycles
		while(stringIter.hasNext() && timer != 10) {
			ArrayList<Object> pair = objectIter.getNextObjs();
			
			// assert that the 0th index contains a Character and 1st index contains a Book
			assertTrue(pair.get(0) instanceof Character);
			assertTrue(pair.get(1) instanceof Book);
			// assert that the size of the list should only be two
			assertTrue(pair.size() == 2);
			
			// prints out the list for reference
			System.out.print("character name: " + ((Character) pair.get(0)).getName() + ", ");
			System.out.println("book title: " + ((Book) pair.get(1)).getTitle());		
			// timer is incremented
			timer++;
		}		
	}
	
	@Test
	public void getNextStringsTest() throws IOException {
		int timer = 0;
		// while iterating allow the iterator to run for only 10 cycles
		while(stringIter.hasNext() && timer != 10) {
			String[] pair = stringIter.getNextStrings();
			
			// assert that the size of the list should only be two
			assertTrue(pair.length == 2);
			
			// prints out the array for reference
			System.out.print("character name: " + pair[0] + ", ");
			System.out.println("book title: " + pair[1]);		
			// timer is incremented
			timer++;
		}		
	}
	
}
