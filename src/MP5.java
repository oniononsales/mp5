import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MP5 {

	public static void main(String[] args) throws InterruptedException,
			IOException, NoPathException {
		int numThreads;
		final String source;
		final String target;
		final String filename;
		List<ArrayList<String>> returnList = new ArrayList<ArrayList<String>>();
		int check = Integer.MAX_VALUE;
		
		filename = args[0];
		source = args[1];
		target = args[2];
		numThreads = new Integer(args[3]).intValue();

		// creates the universe
		final MarvelUniverse theUniverse = new MarvelUniverse();

		// creates an iterator to the file
		Iterator iter = new Iterator(filename);
		// adds all the characters to the universe
		while (iter.hasNext()) {
			// list pair takes the arrayList object containing Character and
			// Book
			List<Object> pair = iter.getNextObjs();
			// creates the key and its associated empty list of relationships
			theUniverse.addCharacter((Character) pair.get(0));
		}

		// **the map now contains all the characters along with its associated
		// empty relationship values of lists

		// adds all the lists of relationships for all the characters from the
		// dataset

		// creates a source iterator for the character
		Iterator sourceIt = new Iterator(filename);
		while (sourceIt.hasNext()) {
			List<Object> sourcePair = sourceIt.getNextObjs();
			Character sourceChar = (Character) sourcePair.get(0);
			Book linkBook1 = (Book) sourcePair.get(1);
			// creates the target iterator for the character
			Iterator targetIt = new Iterator(filename);
			while (targetIt.hasNext()) {
				List<Object> targetPair = targetIt.getNextObjs();
				Character targetChar = (Character) targetPair.get(0);
				Book linkBook2 = (Book) targetPair.get(1);
				// add the relationship only if the source and target are different
				// AND if there is a direct relationship linked by the same book
				if (!sourceChar.equals(targetChar) && linkBook1.equals(linkBook2)) {
					theUniverse.addRelationship(sourceChar, targetChar,
							linkBook1);
				}
			}
		}

		List<Thread> threads = new ArrayList<Thread>();
		for (int ii = 0; ii < numThreads; ++ii) {
			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					Thread.yield();
					try {
						theUniverse.search(source, target);
					} catch (NoSuchCharacterException e) {
						e.printStackTrace();
					}

				}
			});
			t.start();
			threads.add(t);
		}
		// wait for all the threads to finish
		for (Thread t : threads)
			t.join();

		returnList = theUniverse.getReturnList();
		// removes entries lists that are not smallest from overall list
		for (ArrayList<String> tempList : returnList) {

			if (tempList.size() <= check) {
				check = tempList.size();

			}
			if (tempList.size() > check) {
				returnList.remove(tempList);
			}

		}

		if (returnList.size() > 1) {

			ArrayList<String> bestList = returnList.get(0);
			ArrayList<String> tempStore = new ArrayList<String>();

			int size = returnList.size();

			for (int i = 0; i < size; i++) {

				while (returnList.size() > 1) {

					if (!(bestList.equals(returnList.get(i)))) {

						if (checkLex(bestList, returnList.get(i))) {

							returnList.remove(i);

						}
						if (!(checkLex(bestList, returnList.get(i)))) {

							tempStore = returnList.get(i);
							returnList.remove(returnList.indexOf(bestList));
							bestList = tempStore;

						}

					}

				}

			}

			System.out.println(bestList.toString());
			return;

		}

		System.out.println(returnList.toString());
		return;

	}

	/**
	 * Takes a pair of equally sized lists, the first assuming to be the
	 * lexicographically better (appears first in a dictionary) than the second
	 * list with each respective String. Returns true/false based on this
	 * assumption.
	 * 
	 * @param bestList: the assumed lexicographically better list
	 * @param list: the list used to compare bestList
	 * @return: true if the bestList is lexicographically better in respective
	 *          String elements and false otherwise
	 */
	private static boolean checkLex(List<String> bestList,
			List<String> list) {
		// assumptiong starts off true
		boolean isBetter = true;
		// if the two lists are not of equal size then assumption fails due to RI failure
		if(bestList.size() != list.size()) {
			isBetter = false;
		}
		// once size equality passes, checks that each string of bestList is lexicographically better than list
		else for(int index = 0; index < list.size(); index++) {
			// if the current element of list is better than bestList, assumption fails
			if(list.get(index).compareTo(bestList.get(index)) < 0) {
				isBetter = false;
			}
		}
		return isBetter;
	}

}
