import java.io.IOException;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MarvelUniverse {

	// REP INVARIANT: Keys will always be characters, values will be a list of
	// relationships
	// REP INVARIANT: Keys and values will always be non-null
	// REP INVARIANT: marvelUniverse will always be unique to the specific
	// universe
	// ABSTRACTION FUNCTION: represents an undirected graph of characters and
	// relationship
	// in the particular marvel universe
	// THREAD SAFETY ARGUMENT: the graph itself is both private and final, in the method search, a blocking queue and a synchronized list
	// are used to ensure thread safety. In the method getNext, a thread safe synchronized list is used to allow multiple threaded
	// operations.
	//

	// The universe
	private final Map<Character, List<Relationship>> universe;
	// The multiverse will assign unique hashcode ID to the particular universe
	int marvelMultiverse = 0;
	final int marvelUniverse;
	public ArrayList<ArrayList<String>> returnList = new ArrayList<ArrayList<String>>();

	// creates a universe
	public MarvelUniverse() {
		// initializes the universe
		universe = new HashMap<Character, List<Relationship>>();
		// give it an ID
		marvelUniverse = ++marvelMultiverse;
	}

	/**
	 * Add a new character to the universe. If the character already exists in
	 * the universe then this method will return false. Otherwise this method
	 * will add the character to the universe and return true.
	 * 
	 * @param character
	 *            the character to add to the universe. Requires that character
	 *            != null.
	 * @return true if the character was successfully added and false otherwise.
	 * @modifies this by adding the character to the universe if the character
	 *           did not exist in the current universe.
	 */
	public boolean addCharacter(Character character) {
		// check to see if the universe contains the character passed
		if (universe.containsKey(character)) {
			return false;
		}
		// create a new, empty list of relationships as value for the key
		List<Relationship> relationships = new ArrayList<Relationship>();
		// add the character to the universe with the empty list value
		universe.put(character, relationships);
		return true;
	}

	/**
	 * Add a new relationship to the universe. If the relationship already
	 * exists in the universe then this method will return false. Otherwise this
	 * method will add the relationship to the universe and return true.
	 * 
	 * @param character1
	 *            one end of the relationship being added. Requires that !=
	 *            null.
	 * @param character2
	 *            the other end of the relationship being added. Requires that
	 *            != null. Also require that it is not equal to character1
	 *            because narcissism is not meaningful in this universe.
	 * @param book
	 *            the link of the relationship being added. Requires that !=
	 *            null.
	 * @return true if the relationship was successfully added and false
	 *         otherwise.
	 * @modifies this by adding the relationship to the universe if the
	 *           relationship previously did not exist in the universe.
	 */
	public boolean addRelationship(Character character1, Character character2,
			Book linkBook) throws NoPathException {
		// create a new relationship based upon the paramaters passed to this
		// method
		Relationship relationship = new Relationship(character1, character2,
				linkBook);

		// throw no path exception if the characters are the same person
		if (character1.equals(character2)) {
			throw new NoPathException();
		}

		// check to see if the list of relationships associated with the source
		// character
		// already contains this relationship
		List<Relationship> relationships = universe.get(character1);
		for (Relationship tempRelationship : relationships) {
			if (tempRelationship.equals(relationship)) {
				return false;
			}
		}
		// if not add the relationship to the list associated with the target
		universe.get(character1).add(relationship);
		return true;
	}

	/**
	 * Returns the character which matches the given name
	 * 
	 * @param name
	 *            : the character name to search for in the universe.
	 * @return the character that best matches the given search parameter
	 * @throws NoSuchCharacterException
	 *             if the name does not match any character in the universe.
	 * @throws IOException
	 */
	public Character getCharacter(String name) {
		// loops through the universe to search for the character name
		for (Character tempKey : universe.keySet()) {
			String tempName = tempKey.getName();
			// if the character has two names
			if (tempName.contains("/")) {
				// obtain the index at which the substring "/" appears
				int delimIndex = tempName.indexOf("/");
				// the alias name from index 0 to index - 1 of the substring "/"
				String alias = tempName.substring(0, delimIndex);
				// the real name from index + 1 of the substring "/" to index
				// length - 1 of the string
				String realName = tempName.substring(delimIndex + 1);
				// if either the alias or the real name matches the desired
				// name,
				// returns the character object
				if (name.equals(alias) || name.equals(realName)) {
					return tempKey;
				}
			} // if the character only has one name or the given name contains
				// both the alias and the real name
			if (name.equals(tempName)) {
				return tempKey;
			}
		}
		return null;
	}

	/**
	 * Modifier method to add list of paths to another parent list
	 * 
	 * @param source, target
	 *            : the source vertex to start from and the target to look for
	 * @modifies returnList to include various paths from source to target
	 * @throws NoSuchCharacterException if source or target do not exist
	 */
	
	public void search(String source, String target)
			throws NoSuchCharacterException {

		Character sourceChar = getCharacter(source);
		Character targetChar = getCharacter(target);

		if (sourceChar == null || targetChar == null) {
			throw new NoSuchCharacterException();
		}

		BlockingQueue<Character> queue = new ArrayBlockingQueue<Character>(
				universe.get(sourceChar).size());
		List<Character> pathList = Collections
				.synchronizedList(new ArrayList<Character>());

		queue.add(sourceChar);
		pathList.add(sourceChar);

		sourceChar.visited = true;

		while (!queue.isEmpty()) {
			Character tempChar = queue.remove();
			
			if(tempChar.equals(targetChar)){
				pathList.add(tempChar);
				break;
			}

			Character adj = null;

			while ((adj = getNext(tempChar)) != null) {

				adj.visited = true;
				pathList.add(adj);
				queue.add(adj);
			}
		}
		ArrayList<String> pathListString = new ArrayList<String>();
		ListIterator<Character> iter = pathList.listIterator();
		while (iter.hasNext()) {
			pathListString.add(iter.next().getName());
		}

		returnList.add(pathListString);
	}
	
	/**
	 * Looks for next intermediate vertex through relationships
	 * 
	 * @param source
	 *            : the source vertex to start from
	 * @returns: a vertex adjecent to the source vertex that has not been checked,
	 * 			 null if all verticies have been checked already
	 */

	public Character getNext(Character sourceChar) {

		List<Relationship> rList = Collections.synchronizedList(new ArrayList<Relationship>());
		rList.addAll(universe.get(sourceChar));

		for (Relationship tempR : rList) {

			if (!tempR.getTargetCharacter().isVisited()) {
				return tempR.getTargetCharacter();
			}
		}

		return null;
	}
	
	public ArrayList<ArrayList<String>> getReturnList(){
		return returnList;
	}

	@Override
	public boolean equals(Object other) {
		// checks that other is an instance of MarvelUniverse
		if (other instanceof MarvelUniverse) {

			Set<Character> theseCharacters = this.universe.keySet();
			Set<Character> thoseCharacters = ((MarvelUniverse) other).universe
					.keySet();

			// returns true if the numbers of characters are the same for both
			// universe
			// and that the two universe contains the same combinations of
			// characters
			return ((((MarvelUniverse) other).universe.size() == this.universe
					.size()) && (thoseCharacters.containsAll(theseCharacters)));
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		// takes the number of characters in the universe and multiplies
		// it by prime and the marvel universe id
		return marvelUniverse * prime * this.universe.size();
	}
}