import java.io.IOException;
import java.util.List;

public class MarvelUniverseMain {

	public static void main(String[] args) throws IOException, NoPathException {
		
		String filename = "labeled_edges.tsv";

		// creates the universe
		MarvelUniverse theUniverse = new MarvelUniverse();

		// creates an iterator to the file
		Iterator iter = new Iterator(filename);
		// adds all the characters to the universe
		while (iter.hasNext()) {
			// list pair takes the arrayList object containing Character and
			// Book
			List<Object> pair = iter.getNextObjs();
			// creates the key and its associated empty list of relationships
			theUniverse.addCharacter((Character) pair.get(0));
		}

		// **the map now contains all the characters along with its associated
		// empty relationship values of lists

		// adds all the lists of relationships for all the characters from the
		// dataset

		// creates a source iterator for the character
		Iterator sourceIt = new Iterator(filename);
		while (sourceIt.hasNext()) {
			List<Object> sourcePair = sourceIt.getNextObjs();
			Character sourceChar = (Character) sourcePair.get(0);
			Book linkBook1 = (Book) sourcePair.get(1);
			// creates the target iterator for the character
			Iterator targetIt = new Iterator(filename);
			while (targetIt.hasNext()) {
				List<Object> targetPair = targetIt.getNextObjs();
				Character targetChar = (Character) targetPair.get(0);
				Book linkBook2 = (Book) targetPair.get(1);
				// add the relationship only if the source and target are different
				// AND if there is a direct relationship linked by the same book
				if (!sourceChar.equals(targetChar) && linkBook1.equals(linkBook2)) {
					theUniverse.addRelationship(sourceChar, targetChar,
							linkBook1);
				}
			}
		}
	}
}