import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


public class MarvelUniverseTest {

	MarvelUniverse UBC;
	MarvelUniverse SFU;
	Object school;
	Character sathish;
	Character tor;
	Character farshid;
	Character saloome;
	Character berndt;
	Character juicy;
	Character slade;
	Character gateman;
	Character sfuProf1;
	Character sfuProf2;
	Character sfuProf3;
	Book eece;
	Book math;
	Book engineering;
	Book courses;
	
	@Before
	public void setup() throws NoPathException {
		// create two universes
		
		UBC = new MarvelUniverse();
		SFU = new MarvelUniverse();
		
		// create some professors
		sathish = new Character("sathish");
		tor = new Character("tor");
		farshid = new Character("farshid");
		saloome = new Character("saloome");
		berndt = new Character("berndt");
		juicy = new Character("juicy/jcwei");
		slade = new Character("slade");
		gateman = new Character("gateman");
		sfuProf1 = new Character("prof1");
		sfuProf2 = new Character("prof2");
		sfuProf3 = new Character("prof3");
		
		// create associated books
		eece = new Book("eece");
		math = new Book("math");
		engineering = new Book("engineering");
		courses = new Book("courses");
		
		// add the engineering courses professors to UBC
		UBC.addCharacter(sathish);
		UBC.addCharacter(tor);
		UBC.addCharacter(farshid);
		UBC.addCharacter(saloome);
		UBC.addCharacter(berndt);
		UBC.addCharacter(juicy);
		UBC.addCharacter(slade);
		
		// add the generic profs to SFU
		SFU.addCharacter(sfuProf1);
		SFU.addCharacter(sfuProf2);
		SFU.addCharacter(sfuProf3);

		// add the all the relationships between sathish, tor, farshid and saloome; the eece professors
		// for sathish
		UBC.addRelationship(sathish, tor, eece);
		UBC.addRelationship(sathish, farshid, eece);
		UBC.addRelationship(sathish, saloome, eece);
		// for tor
		UBC.addRelationship(tor, sathish, eece);
		UBC.addRelationship(tor, farshid, eece);
		UBC.addRelationship(tor, saloome, eece);
		// for farshid
		UBC.addRelationship(farshid, sathish, eece);
		UBC.addRelationship(farshid, tor, eece);
		UBC.addRelationship(farshid, saloome, eece);
		// for saloome
		UBC.addRelationship(saloome, sathish, eece);
		UBC.addRelationship(saloome, tor, eece);
		UBC.addRelationship(saloome, farshid, eece);
	}
	
	@Test
	public void addCharacterPass() {
		// passes when the character added is unique to the graph; not an engineering professor
		assertTrue(UBC.addCharacter(gateman));
	}
	
	@Test
	public void addCharacterFail() {
		// fails when the character added is a duplicate to the graph; a previous engineering professor
		Character sathishDuplicate = new Character("sathish");
		assertFalse(UBC.addCharacter(sathishDuplicate));
	}
	
	@Test
	public void addRelationshipPass() throws NoPathException {
		// passes when the relationship added is unique to the character
		// the linkBook is unique
		assertTrue(UBC.addRelationship(sathish, tor, engineering));
	}
	
	@Test
	public void addRelationshipFail() throws NoPathException {
		// fails when the relationship added is a duplicate to the character
		// the relationship is a duplicate in equality
		assertFalse(UBC.addRelationship(sathish, tor, eece));
		
	}
	
	@Test
	public void addRelationshipNoPath() throws NoPathException {
		// an exception should be thrown so the test can pass
		try {
			UBC.addRelationship(sathish, sathish, eece);
			fail("There is no path to this relationship!");
		} catch (NoPathException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void getCharacterTest() {
		// the character can be obtained by calling get character
		assertEquals(sathish, UBC.getCharacter("sathish"));
	}
	
	@Test
	public void getCharacterDual() {
		// the character can be obtained even if the input is a substring of the complete name
		assertEquals(juicy, UBC.getCharacter("juicy"));
		assertEquals(juicy, UBC.getCharacter("jcwei"));
		assertEquals(juicy, UBC.getCharacter("juicy/jcwei"));
	}
	
	@Test
	public void getCharacterNull() {
		// the character obtained is null pointer because the character does not exist in UBC
		assertTrue(UBC.getCharacter("Justin Bieber") == null);
	}
	
	@Test
	public void searchTest() throws NoSuchCharacterException {
		UBC.search("sathish", "tor");
		ArrayList<String> pathList = UBC.getReturnList().get(UBC.getReturnList().size() - 1);
		
		// the first String in the pathList should be the source String
		assertEquals(pathList.get(0), "sathish");
		// the last String in the pathList should be the target String
		assertEquals(pathList.get(pathList.size() - 1), "tor");

	}
	
	@Test
	public void getNextChar() {
		// return the earliest adjacent relationship character
		assertEquals(UBC.getNext(sathish), tor);
		assertEquals(UBC.getNext(tor), sathish);
		assertEquals(UBC.getNext(saloome), sathish);
		assertEquals(UBC.getNext(farshid), sathish);
	}
	
	@Test
	public void getNextNull() {
		// there should not be any adjacent character to slade because he currently does not
		// have any relationship with other characters
		assertTrue(UBC.getNext(slade) == null);
	}
	
	@Test
	public void equalsTrue() {
		// UBC is equal to itself
		assertTrue(UBC.equals(UBC));
	}
	
	@Test
	public void equalsFalse() {
		// UBC is not the same as SFU
		assertFalse(UBC.equals(SFU));
	}
	
	@Test
	public void equalsNotInstanceOf() {
		// UBC is not equatable with school
		assertFalse(UBC.equals(school));
	}
	
	@Test
	public void hashCodeTest1() {
		// the hashcode of UBC by itself will be equivalent and consistent
		assertTrue(UBC.hashCode() == UBC.hashCode());
	}
	
	@Test
	public void hashCodeTest2() {
		// the hashcode of UBC and SFU will not be equivalent
		assertFalse(UBC.hashCode() == SFU.hashCode());
	}
}
