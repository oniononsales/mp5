public class Relationship {
	private final Character sourceCharacter;
	private final Character targetCharacter;
	private final Book linkBook;

	public Relationship(Character sourceCharacter, Character targetCharacter, Book linkBook) {
		this.sourceCharacter = sourceCharacter;
		this.targetCharacter = targetCharacter;
		this.linkBook = linkBook;
	}

	public Character getSourceCharacter() {
		return sourceCharacter;
	}

	public Character getTargetCharacter() {
		return targetCharacter;
	}

	public Book getLinkBook() {
		return linkBook;
	}

	@Override
	public boolean equals(Object other) {
		// checks if other is an instance of Edge
		if (other instanceof Relationship) {
			// return true if the source character, target character, and link book all match correspondingly
			return (this.getSourceCharacter().equals(
					((Relationship) other).getSourceCharacter())
					&& this.getTargetCharacter().equals(
							((Relationship) other).getTargetCharacter()) && this
					.getLinkBook().equals(((Relationship) other).getLinkBook()));
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 47;
		// takes the length of the source character, target character, and book title strings
		// and multiply by prime
		return prime * this.getSourceCharacter().getName().toCharArray().length
				* this.getTargetCharacter().getName().toCharArray().length
				* this.getLinkBook().getTitle().toCharArray().length;
	}
}